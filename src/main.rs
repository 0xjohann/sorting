use sdl2;

use sdl2::pixels::Color;
use sdl2::rect::Rect;
use std::io::{stdout, Write};
use std::sync::{Arc, RwLock};
use std::thread;
use std::time::{Duration, Instant};

mod bar;
mod thread_blocker;
use bar::Bar;
mod sorting_algorithms;
use sorting_algorithms::*;

use rand::seq::SliceRandom;
use rand::thread_rng;

use crate::thread_blocker::{BlockError, ThreadBlocker};

const SCREEN_WIDTH: u32 = 1000;
const SCREEN_HEIGHT: u32 = 850;
const NUMBER_OF_BARS: usize = 1000;
const BAR_WIDTH: f32 = SCREEN_WIDTH as f32 / NUMBER_OF_BARS as f32;

fn decimal_to_rgb(value: u64, max_value: u64) -> (u8, u8, u8) {
    let hue = (value as f64 / max_value as f64) * 360.0;

    let saturation: f64 = 1.0;
    let lightness: f64 = 0.5;

    let c = (1.0 - (2.0 * lightness - 1.0).abs()) * saturation;
    let x = c * (1.0 - ((hue / 60.0) % 2.0 - 1.0).abs());
    let m = lightness - c / 2.0;

    let (r, g, b) = if hue >= 0.0 && hue < 60.0 {
        ((c + m) * 255.0, (x + m) * 255.0, (m) * 255.0)
    } else if hue >= 60.0 && hue < 120.0 {
        ((x + m) * 255.0, (c + m) * 255.0, (m) * 255.0)
    } else if hue >= 120.0 && hue < 180.0 {
        ((m) * 255.0, (c + m) * 255.0, (x + m) * 255.0)
    } else if hue >= 180.0 && hue < 240.0 {
        ((m) * 255.0, (x + m) * 255.0, (c + m) * 255.0)
    } else if hue >= 240.0 && hue < 300.0 {
        ((x + m) * 255.0, (m) * 255.0, (c + m) * 255.0)
    } else {
        ((c + m) * 255.0, (m) * 255.0, (x + m) * 255.0)
    };

    (r as u8, g as u8, b as u8)
}

fn render_bars(
    renderer: &mut sdl2::render::Canvas<sdl2::video::Window>,
    array: &[Bar],
    current_index: usize,
) {
    renderer.set_draw_color(Color::RGB(0, 0, 0));
    renderer.clear();

    for (i, &ref bar) in array.iter().enumerate() {
        let x = i as i32 * BAR_WIDTH.floor() as i32;
        let y = (SCREEN_HEIGHT - bar.height) as i32;

        // Check if the current bar is the one being processed and change its color to green
        if i == current_index || i == current_index + 1 && !(current_index < 1) {
            renderer.set_draw_color(Color::RGB(255, 255, 255)); // Green color
        } else {
            renderer.set_draw_color(Color::RGB(bar.rgb.0, bar.rgb.1, bar.rgb.2));
            // Blue color for other bars
        }

        let rect = Rect::new(x, y, BAR_WIDTH.floor() as u32, bar.height);
        renderer
            .fill_rect(rect)
            .expect("Failed to draw a rectangle");
    }

    renderer.present();
}

fn randomize_vector<T>(vec: &mut Vec<T>) {
    let mut rng = thread_rng();
    vec.shuffle(&mut rng);
}

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window(
            "Sorting Algorithm Visualization",
            SCREEN_WIDTH,
            SCREEN_HEIGHT,
        )
        .position_centered()
        .build()
        .unwrap();

    let mut renderer = window
        .into_canvas()
        //.present_vsync()
        .build()
        .unwrap();

    // FIX VISUAL BUG!

    let mut array: Vec<Bar> = (1..=NUMBER_OF_BARS)
        .map(|x| {
            Bar::new(
                x as u32,
                ((x as f32 / NUMBER_OF_BARS as f32) * SCREEN_HEIGHT as f32).round() as u32,
                decimal_to_rgb(x as u64, NUMBER_OF_BARS as u64),
            )
        })
        .collect();

    randomize_vector(&mut array);

    let array = Arc::new(RwLock::new(&mut array[..]));

    thread::scope(|scope| {
        let thread_blocker = ThreadBlocker::new();
        let blocker = thread_blocker.blocker;
        let unblocker = thread_blocker.unblocker;

        let cloned = array.clone();

        scope.spawn(|| {
            //bubble_sort(cloned, blocker);
            quick_sort(cloned, blocker);
            //bogo_sort(cloned, blocker);
            //selection_sort(cloned, blocker);
        });

        let fps_thread = ThreadBlocker::new();
        let fps_blocker = fps_thread.blocker;
        let fps_unblocker = fps_thread.unblocker;
        scope.spawn(|| {
            let fps_blocker = fps_blocker;

            let mut stdout = stdout();
            let mut number_of_generated_frames: f64 = 0.0;

            let started = Instant::now();

            loop {
                let now = Instant::now();
                for i in 0..1000 {
                    let time_elapsed_this_frame = now.elapsed().as_secs_f64();
                    number_of_generated_frames += 1.0;
                    let current_fps = i as f64 / time_elapsed_this_frame;
                    let average_fps = number_of_generated_frames / started.elapsed().as_secs_f64();

                    match fps_blocker.block_thread((current_fps, average_fps)) {
                        Ok(()) => (),
                        Err(_e) => return,
                    }

                    stdout.flush().unwrap();
                }
            }
        });

        let mut event_pump = sdl_context.event_pump().unwrap();
        'main: loop {
            for event in event_pump.poll_iter() {
                match event {
                    sdl2::event::Event::Quit { .. } => {
                        if let Ok(_send_error) = unblocker.unblock(Err(BlockError::Quit)) {}
                        if let Ok(_send_error) = fps_unblocker.unblock(Err(BlockError::Quit)) {}
                        break 'main;
                    }
                    _ => {}
                }
            }

            // render window contents here
            {
                if let Ok(current_index) = unblocker.receive_data() {
                    render_bars(&mut renderer, &*array.read().unwrap(), current_index);
                    unblocker.unblock(Ok(())).unwrap();
                    thread::sleep(Duration::from_millis(0));
                }
            }

            // Get FPS from other Thread
            if let Ok((current_fps, average_fps)) = fps_unblocker.receive_data() {
                print!("\rcurrent FPS: {current_fps:.2} | average FPS: {average_fps:.2}");
                fps_unblocker.unblock(Ok(())).unwrap();
            }
        }

        println!();
    });
}
