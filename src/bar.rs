#[derive(Eq, Debug, Clone)]
pub struct Bar {
    pub value: u32,
    pub height: u32,
    pub rgb: (u8, u8, u8),
}

impl Bar {
    pub fn new(value: u32, height: u32, rgb: (u8, u8, u8)) -> Self {
        Bar { value, height, rgb }
    }
}

impl Ord for Bar {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.value.cmp(&other.value)
    }
}

impl PartialOrd for Bar {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Bar {
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value
    }
}
