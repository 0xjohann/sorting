#![allow(dead_code)]
use crate::bar::Bar;
use crate::thread_blocker::{BlockComponent, BlockError};
use rand::prelude::SliceRandom;
use std::sync::{Arc, RwLock};

fn partition(
    arr: &Arc<RwLock<&mut [Bar]>>,
    low: usize,
    high: usize,
    thread_blocker: &BlockComponent<usize>,
) -> Result<usize, BlockError> {
    let pivot = arr.read().unwrap()[high].value;
    let mut i = low;
    for j in low..high {
        if arr.read().unwrap()[j].value < pivot {
            arr.write().unwrap().swap(i, j);

            match thread_blocker.block_thread(j) {
                Ok(()) => (),
                Err(e) => return Err(e),
            }

            i += 1;
        }
    }
    arr.write().unwrap().swap(i, high);
    Ok(i)
}

pub fn quick_sort(arr: Arc<RwLock<&mut [Bar]>>, thread_blocker: BlockComponent<usize>) {
    let mut stack: Vec<(usize, usize)> = Vec::new();
    let len = arr.read().unwrap().len();
    stack.push((0, len - 1));

    while let Some((low, high)) = stack.pop() {
        if low < high {
            let pivot_idx = if let Ok(num) = partition(&arr, low, high, &thread_blocker) {
                num
            } else {
                return;
            };
            if pivot_idx > 0 {
                stack.push((low, pivot_idx - 1));
            }
            stack.push((pivot_idx + 1, high));
        }
    }
}

pub fn bubble_sort(array: Arc<RwLock<&mut [Bar]>>, thread_blocker: BlockComponent<usize>) {
    let n = array.read().unwrap().len();
    for i in 0..n {
        for j in 0..n - i - 1 {
            if array.read().unwrap()[j].value > array.read().unwrap()[j + 1].value {
                array.write().unwrap().swap(j, j + 1);

                match thread_blocker.block_thread(j) {
                    Ok(()) => (),
                    Err(_e) => return,
                }
            }
        }
    }
}

pub fn selection_sort(arr: Arc<RwLock<&mut [Bar]>>, thread_blocker: BlockComponent<usize>) {
    let n = arr.read().unwrap().len();

    for i in 0..n {
        let mut min_index = i;

        for j in (i + 1)..n {
            if arr.read().unwrap()[j].value < arr.read().unwrap()[min_index].value {
                min_index = j;
            }
        }

        if i != min_index {
            arr.write().unwrap().swap(i, min_index);

            match thread_blocker.block_thread(i) {
                Ok(()) => (),
                Err(_e) => return,
            }
        }
    }
}

fn is_sorted(arr: &[Bar]) -> bool {
    for i in 1..arr.len() {
        if arr[i - 1].value > arr[i].value {
            return false;
        }
    }
    true
}

pub fn bogo_sort(arr: Arc<RwLock<&mut [Bar]>>, thread_blocker: BlockComponent<usize>) {
    let mut rng = rand::thread_rng();

    while !is_sorted(&arr.read().unwrap()) {
        arr.write().unwrap().shuffle(&mut rng);

        match thread_blocker.block_thread(0) {
            Ok(()) => (),
            Err(_e) => return,
        }
    }
}
