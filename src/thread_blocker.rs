use std::sync::mpsc::{channel, Receiver, RecvError, SendError, Sender};

#[derive(Debug, Clone)]
pub enum BlockError {
    Quit,
}

pub struct ThreadBlocker<T> {
    pub blocker: BlockComponent<T>,
    pub unblocker: UnblockComponent<T>,
}

impl<T> ThreadBlocker<T> {
    pub fn new() -> Self {
        let (tx_for_blocker, rx_for_blocker): (Sender<T>, Receiver<T>) = channel();
        let (tx_for_unblocker, rx_for_unblocker) = channel();

        let blocker = BlockComponent::new(tx_for_blocker, rx_for_unblocker);
        let unblocker = UnblockComponent::new(rx_for_blocker, tx_for_unblocker);

        Self { blocker, unblocker }
    }
}

pub struct UnblockComponent<T> {
    receiver: Receiver<T>,
    unblocker: Sender<Result<(), BlockError>>,
}

impl<T> UnblockComponent<T> {
    fn new(receiver: Receiver<T>, unblocker: Sender<Result<(), BlockError>>) -> Self {
        Self {
            receiver,
            unblocker,
        }
    }

    pub fn unblock(
        &self,
        info: Result<(), BlockError>,
    ) -> Result<(), SendError<Result<(), BlockError>>> {
        self.unblocker.send(info)?;
        Ok(())
    }

    pub fn receive_data(&self) -> Result<T, RecvError> {
        self.receiver.recv()
    }
}

pub struct BlockComponent<T> {
    sender: Sender<T>,
    waiter: Receiver<Result<(), BlockError>>,
}

impl<T> BlockComponent<T> {
    fn new(sender: Sender<T>, waiter: Receiver<Result<(), BlockError>>) -> Self {
        BlockComponent { sender, waiter }
    }

    pub fn block_thread(&self, value: T) -> Result<(), BlockError> {
        if let Err(_e) = self.sender.send(value) {
            return Err(BlockError::Quit);
        }

        if let Err(_e) = self.waiter.recv().unwrap() {
            return Err(BlockError::Quit);
        }

        Ok(())
    }
}
